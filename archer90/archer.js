/*
CopyRight Vic Cekvenich - 2016- all rights reserved
Licensed under these terms only: http://creativecommons.org/licenses/by-sa/4.0
If you require an alternative email vin(at)eml.cc

Based on http://github.com/ryan-schroeder/pug-static/blob/master/lib/pug-static.js

Installation steps:
 npm install pug
 npm install jstransformer-markdown-it
 node archer
 Open browser and navigate to http://localhost:3002/blog.pug

*/
const express = require('express')
const path 	= require ('path')
const pug 	= require('pug')
const fs 	= require('fs')
const md 	= require('jstransformer')(require('jstransformer-markdown-it'))

const exp 	= express()

// -- CONFIG --
var OPTIONS = {}
OPTIONS.PATH = path.join(__dirname, 'www')

var PUG = {}
PUG.pretty=true

// -- CODE --
function returnPug(req, resp, next) {
	var fullPath = path.join(OPTIONS.PATH, req.url)
	//console.log(fullPath)
	fs.readFile(fullPath,'utf8', function (err, content) {
		if (err != null) { console.log('rp '+err) } //err
		try {
			var rendered = pug.render(content, PUG)
			//console.log(rendered)
			return resp.send(rendered, {'Content-Type': 'text/html'}, 200)
		} catch (error) { console.log('re ' +err)} //err
	})//end read file
}//()

exp.get('*',function(req, resp, next) {
	console.log(path.extname(req.url)) // what is this file type?

	returnPug(req, resp, next)
})
exp.use(express.static(OPTIONS.PATH))

// -- START --
var srv = exp.listen(3002,'0.0.0.0')
